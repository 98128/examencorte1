package com.example.examen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class ReciboNominaActivity extends AppCompatActivity {
    private TextView lblTitulo;
    private TextView lblNumRecibo;
    private EditText txtNumRecibo;
    private TextView lblNombre2;
    private TextView txtNombre2;
    private TextView lblHorasNorm;
    private EditText txtHorasNorm;
    private TextView lblHorasEx;
    private EditText txtHorasEx;
    private RadioGroup radioG;
    private RadioButton radioBtn1;
    private RadioButton radioBtn2;
    private RadioButton radioBtn3;
    private TextView lblSubtotal;
    private TextView txtSubtotal;
    private TextView lblImpuesto;
    private TextView txtImpuesto;
    private TextView lblTotal;
    private TextView txtTotal;
    private Button btnCalcular;
    private Button btnLimpiar;
    private Button btnRegresar;
    private ReciboNomina reciboNomina;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibo_nomina);

        lblTitulo = findViewById(R.id.lblTitulo);
        lblNumRecibo = findViewById(R.id.lblNumRecibo);
        txtNumRecibo = findViewById(R.id.txtNumRecibo);
        lblNombre2 = findViewById(R.id.lblNombre2);
        txtNombre2 = findViewById(R.id.txtNombre2);
        lblHorasNorm = findViewById(R.id.lblHorasNorm);
        txtHorasNorm = findViewById(R.id.txtHorasNorm);
        lblHorasEx = findViewById(R.id.lblHorasEx);
        txtHorasEx = findViewById(R.id.txtHorasEx);
        radioG = findViewById(R.id.radioG);
        radioBtn1 = findViewById(R.id.radioBtn1);
        radioBtn2 = findViewById(R.id.radioBtn2);
        radioBtn3 = findViewById(R.id.radioBtn3);
        lblSubtotal = findViewById(R.id.lblSubtotal);
        txtSubtotal = findViewById(R.id.txtSubtotal);
        lblImpuesto = findViewById(R.id.lblImpuesto);
        txtImpuesto = findViewById(R.id.txtImpuesto);
        lblTotal = findViewById(R.id.lblTotal);
        txtTotal = findViewById(R.id.txtTotal);
        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnRegresar = findViewById(R.id.btnRegresar);

        Bundle datos = getIntent().getExtras();

        reciboNomina = (ReciboNomina) datos.getSerializable("reciboNomina");

        txtNombre2.setText(reciboNomina.getNombre());

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String horasNorm = txtHorasNorm.getText().toString();
                String horasEx = txtHorasEx.getText().toString();

                if (horasNorm.matches("") || horasEx.matches("")){
                    Toast.makeText(ReciboNominaActivity.this,"Ingrese los datos solicitados", Toast.LENGTH_SHORT).show();
                }
                else{
                    reciboNomina.setHorasTrabNormal(Double.parseDouble(txtHorasNorm.getText().toString()));
                    reciboNomina.setHorasTrabExtra(Double.parseDouble(txtHorasEx.getText().toString()));
                    reciboNomina.setImpuestoPorc(16);
                    reciboNomina.setNumRecibo(Integer.parseInt(txtNumRecibo.getText().toString()));
                    switch (radioG.getCheckedRadioButtonId()){
                        case R.id.radioBtn1: reciboNomina.setPuesto(1);
                        break;

                        case R.id.radioBtn2: reciboNomina.setPuesto(2);
                        break;

                        case R.id.radioBtn3: reciboNomina.setPuesto(3);
                        break;
                    }

                    txtSubtotal.setText(String.valueOf(reciboNomina.calcularSubtotal()));
                    txtImpuesto.setText(String.valueOf(reciboNomina.calcularImpuesto()));
                    txtTotal.setText(String.valueOf(reciboNomina.calcularTotal()));
                }
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtHorasNorm.setText("");
                txtHorasEx.setText("");
                txtSubtotal.setText("");
                txtImpuesto.setText("");
                txtTotal.setText("");
                radioG.check(R.id.radioBtn1);
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent2 = new Intent(ReciboNominaActivity.this,MainActivity.class);

                startActivity(intent2);
                finish();
            }
        });
    }
}
