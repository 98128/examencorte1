package com.example.examen;

import java.io.Serializable;

public class ReciboNomina implements Serializable {

    private int numRecibo;
    private String nombre;
    private double horasTrabNormal;
    private double horasTrabExtra;
    private int puesto;
    private double impuestoPorc;

    public ReciboNomina() {
    }

    public ReciboNomina(int numRecibo, String nombre, double horasTrabNormal, double horasTrabExtra, int puesto, double impuestoPorc) {
        this.numRecibo = numRecibo;
        this.nombre = nombre;
        this.horasTrabNormal = horasTrabNormal;
        this.horasTrabExtra = horasTrabExtra;
        this.puesto = puesto;
        this.impuestoPorc = impuestoPorc;
    }

    public int getNumRecibo() {
        return numRecibo;
    }

    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getHorasTrabNormal() {
        return horasTrabNormal;
    }

    public void setHorasTrabNormal(double horasTrabNormal) {
        this.horasTrabNormal = horasTrabNormal;
    }

    public double getHorasTrabExtra() {
        return horasTrabExtra;
    }

    public void setHorasTrabExtra(double horasTrabExtra) {
        this.horasTrabExtra = horasTrabExtra;
    }

    public int getPuesto() {
        return puesto;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }

    public double getImpuestoPorc() {
        return impuestoPorc;
    }

    public void setImpuestoPorc(double impuestoPorc) {
        this.impuestoPorc = impuestoPorc;
    }

    public double calcularSubtotal(){
        double pago=0;
        switch(puesto){
            case 1: pago = (horasTrabNormal*50)+(horasTrabExtra*100);
            break;

            case 2: pago = (horasTrabNormal*70)+(horasTrabExtra*140);
            break;

            case 3: pago = (horasTrabNormal*100)+(horasTrabExtra*200);
            break;
        }

        return pago;
    }

    public double calcularImpuesto(){
        double subTotal = calcularSubtotal();

        double impuesto = (subTotal*impuestoPorc)/100;

        return impuesto;
    }

    public double calcularTotal(){
        double Total = calcularSubtotal() - calcularImpuesto();
        return Total;
    }
}
