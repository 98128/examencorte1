package com.example.examen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private TextView lblHeader;
    private TextView lblNombre;
    private EditText txtNombre;
    private ImageView imgLogo;
    private Button btnEntrar;
    private Button btnSalir;
    private ReciboNomina reciboNomina = new ReciboNomina();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lblHeader = findViewById(R.id.lblHeader);
        lblNombre = findViewById(R.id.lblNombre);
        txtNombre = findViewById(R.id.txtNombre);
        imgLogo = findViewById(R.id.imgLogo);
        btnEntrar = findViewById(R.id.btnEntrar);
        btnSalir = findViewById(R.id.btnSalir);

        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nombre = txtNombre.getText().toString();
                if (nombre.matches("")){
                    Toast.makeText(MainActivity.this,"Ingrese los datos solicitados", Toast.LENGTH_SHORT).show();
                }
                else{
                    reciboNomina.setNombre(txtNombre.getText().toString());
                    Intent intent = new Intent(MainActivity.this,ReciboNominaActivity.class);
                    Bundle objeto = new Bundle();
                    objeto.putSerializable("reciboNomina", reciboNomina);

                    intent.putExtras(objeto);

                    startActivity(intent);
                    finish();
                }
            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }






















    /*enviar*/

        /*Bundle objeto = new Bundle();
        objeto.putSerializable("cotizacion",cotizacion); //proceso de empaquetado

        intent.putExtras(objeto); //dar "objeto" a intent para enviarlo

        startActivity(intent);*/

    /*recibir*/

        /*Bundle datos = getIntent().getExtras(); // creacion del objeto empaquetado y colocacion de datos

        lblNombre.setText(datos.getString("cliente")); //asignacion de datos del paquete (Bundle)

        cotizacion = (Cotizacion) datos.getSerializable("cotizacion"); //asignacion de datos del objeto

        lblFolio.setText(String.valueOf(cotizacion.folioGenerate()));
        */

}
